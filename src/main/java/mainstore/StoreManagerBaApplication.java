package mainstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreManagerBaApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoreManagerBaApplication.class, args);
	}

}
